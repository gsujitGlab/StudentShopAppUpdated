import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testing123/Categories/categories.dart';
import 'ProductFile/products.dart';
import 'file:///C:/Users/Victor%20Polisetty/Desktop/Programming/StudentShopApp-main/lib/Widgets/searchBarWidget.dart';
import 'file:///C:/Users/Victor%20Polisetty/Desktop/Programming/StudentShopApp-main/lib/Widgets/sectionTitleHeader.dart';

class MyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SearchBarWidget(),
          SectionTitle(title: "Featured"
          ),
          Products(),
          SectionTitle(title: "Categories"
          ),
          Categories(),
          SectionTitle(title: "New Arrival"
          ),
          Products(),
        ],
      )
    );
  }
}
