import 'package:flutter/material.dart';
import 'package:testing123/Widgets/newHomePage.dart';
import 'package:testing123/pages/favoritePage.dart';
import 'ProfileFile/sellerShop.dart';
import 'package:firebase_core/firebase_core.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final List<Widget> tabs = [
    newHomePage(),
    favoritePage(),
    sellerShop(),
  ];
  @override
  Widget build(BuildContext context) {
    Firebase.initializeApp();
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body:
          tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),

          BottomNavigationBarItem(icon: Icon(Icons.favorite,), title: Text("My Favorites"),),

          BottomNavigationBarItem(icon: Icon(Icons.person), title: Text("Profile")),
        ],
        onTap: (index){
          setState(() {
            _currentIndex = index;
          });
        },
        selectedItemColor: Colors.black,),
    );
  }
}
