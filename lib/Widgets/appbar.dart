import 'package:flutter/material.dart';
import 'package:testing123/ProfileFile/sellerShop.dart';

class MyAppBar extends StatefulWidget {
  @override
  _MyAppBarState createState() => _MyAppBarState();
}

class _MyAppBarState extends State<MyAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: new IconThemeData(color: Colors.grey[800], size: 27),
      backgroundColor: Colors.grey[200],
      elevation: 0,
      title: Center(
        child: Text(
          'Student Shop',
          style: TextStyle(color:Colors.black),
        ),
      ),
      actions: [
        Container(
          margin: EdgeInsets.only(right: 10),
          child: Icon(
            Icons.notifications,
            color:Colors.grey[800],
            size: 27,
          ),
        ),
        InkWell(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => sellerShop()),
            );
          },
          child: Container(
            margin: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.shopping_cart,
              color:Colors.grey[800],
              size: 27,
            ),
          ),
        ),
      ],
    );
  }
}
