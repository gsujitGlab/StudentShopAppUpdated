import 'package:flutter/material.dart';
import 'package:testing123/Widgets/appbar.dart';
import 'package:testing123/myBody.dart';

class newHomePage extends StatefulWidget {
  @override
  _newHomePageState createState() => _newHomePageState();
}

class _newHomePageState extends State<newHomePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          MyAppBar(),
          MyBody(),
          // _children[_currentIndex]
        ],
      ),
    );
  }
}
